import 'package:GLF/Entry/SignupScreen.dart';
import 'package:GLF/Entry/SocailIcon.dart';

import 'package:GLF/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_sign_in/google_sign_in.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _isSelected = false;
  bool isLoading = false;
  String _email, _password;
  final GlobalKey<FormState> _formkey = GlobalKey<FormState>();

  void _radio() {
    setState(() {
      _isSelected = !_isSelected;
    });
  }

  Widget radioButton(bool isSelected) => Container(
        width: 16.0,
        height: 16.0,
        padding: EdgeInsets.all(2.0),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(width: 2.0, color: Colors.black)),
        child: isSelected
            ? Container(
                width: double.infinity,
                height: double.infinity,
                decoration:
                    BoxDecoration(shape: BoxShape.circle, color: Colors.black),
              )
            : Container(),
      );

  Widget horizontalLine() => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          width: ScreenUtil.getInstance().setWidth(120),
          height: 1.0,
          color: Colors.black26.withOpacity(.2),
        ),
      );
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance =
        ScreenUtil(width: 750, height: 1334, allowFontScaling: true);
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: true,
        body: isLoading == false
            ? Stack(
                // key: _key,
                fit: StackFit.expand,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        // child: Image.asset("assets/image_04.jpg"),
                      ),
                      Expanded(
                        child: Container(),
                      ),
                      // Image.asset("assets/image_02.jpg")
                    ],
                  ),
                  SingleChildScrollView(
                    child: Padding(
                      padding:
                          EdgeInsets.only(left: 28.0, right: 28.0, top: 60.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Image.asset(
                                "assets/icon.jpg",
                                width: ScreenUtil.getInstance().setWidth(110),
                                height: ScreenUtil.getInstance().setHeight(110),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Galle\nLiterary Festival",
                                    style: TextStyle(
                                        fontFamily: "Poppins-Bold",
                                        fontSize:
                                            ScreenUtil.getInstance().setSp(46),
                                        letterSpacing: .6,
                                        fontWeight: FontWeight.bold)),
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(180),
                          ),
                          // FormCard(),

                          Form(
                            key: _formkey,
                            child: new Container(
                              width: double.infinity,
                              height: ScreenUtil.getInstance().setHeight(600),
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(8.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black12,
                                        offset: Offset(0.0, 15.0),
                                        blurRadius: 15.0),
                                    BoxShadow(
                                        color: Colors.black12,
                                        offset: Offset(0.0, -10.0),
                                        blurRadius: 10.0),
                                  ]),
                              child: Padding(
                                padding: EdgeInsets.only(
                                    left: 16.0, right: 16.0, top: 16.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Login",
                                        style: TextStyle(
                                            fontSize: ScreenUtil.getInstance()
                                                .setSp(45),
                                            fontFamily: "Poppins-Bold",
                                            letterSpacing: .6)),
                                    SizedBox(
                                      height: ScreenUtil.getInstance()
                                          .setHeight(30),
                                    ),
                                    Text("Email",
                                        style: TextStyle(
                                            fontFamily: "Poppins-Medium",
                                            fontSize: ScreenUtil.getInstance()
                                                .setSp(26))),
                                    TextFormField(
                                      decoration: InputDecoration(
                                          hintText: "email",
                                          hintStyle: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12.0)),
                                      validator: (input) {
                                        if (input.isEmpty) {
                                          return "Please type your email";
                                        }
                                      },
                                      onSaved: (input) => _email = input,
                                    ),
                                    SizedBox(
                                      height: ScreenUtil.getInstance()
                                          .setHeight(30),
                                    ),
                                    Text("Password",
                                        style: TextStyle(
                                            fontFamily: "Poppins-Medium",
                                            fontSize: ScreenUtil.getInstance()
                                                .setSp(26))),
                                    TextFormField(
                                      obscureText: true,
                                      decoration: InputDecoration(
                                          hintText: "Password",
                                          hintStyle: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12.0)),
                                      validator: (input) {
                                        if (input.length < 8) {
                                          return "Your password needs to be alteast 8 characters";
                                        }
                                      },
                                      onSaved: (input) => _password = input,
                                    ),
                                    SizedBox(
                                      height: ScreenUtil.getInstance()
                                          .setHeight(35),
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Text(
                                          "Forgot Password?",
                                          style: TextStyle(
                                              color: Colors.blue,
                                              fontFamily: "Poppins-Medium",
                                              fontSize: ScreenUtil.getInstance()
                                                  .setSp(28)),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                              height: ScreenUtil.getInstance().setHeight(40)),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 12.0,
                                  ),
                                  GestureDetector(
                                    onTap: _radio,
                                    child: radioButton(_isSelected),
                                  ),
                                  SizedBox(
                                    width: 8.0,
                                  ),
                                  Text("Remember me",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontFamily: "Poppins-Medium"))
                                ],
                              ),
                              InkWell(
                                child: Container(
                                  width: ScreenUtil.getInstance().setWidth(330),
                                  height:
                                      ScreenUtil.getInstance().setHeight(100),
                                  decoration: BoxDecoration(
                                      gradient: LinearGradient(colors: [
                                        Color(0xFF17ead9),
                                        Color(0xFF6078ea)
                                      ]),
                                      borderRadius: BorderRadius.circular(6.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Color(0xFF6078ea)
                                                .withOpacity(.3),
                                            offset: Offset(0.0, 8.0),
                                            blurRadius: 8.0)
                                      ]),
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        login();
                                      },
                                      child: Center(
                                        child: Text("SIGN IN",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Poppins-Bold",
                                                fontSize: 18,
                                                letterSpacing: 1.0)),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(40),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              horizontalLine(),
                              Text("Social Login",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontFamily: "Poppins-Medium")),
                              horizontalLine()
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(40),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SocialIcon(
                                  colors: [
                                    Color(0xFF102397),
                                    Color(0xFF187adf),
                                    Color(0xFF00eaf8),
                                  ],
                                  iconData: CustomIcons.googlePlus,
                                  onPressed: () {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    googleLogin();
                                  }),
                              SocialIcon(
                                colors: [
                                  Color(0xFFff4f38),
                                  Color(0xFFff355d),
                                ],
                                iconData: CustomIcons.facebook,
                                onPressed: () {},
                              ),
                              SocialIcon(
                                colors: [
                                  Color(0xFF17ead9),
                                  Color(0xFF6078ea),
                                ],
                                iconData: CustomIcons.twitter,
                                onPressed: () {},
                              ),
                              SocialIcon(
                                colors: [
                                  Color(0xFF00c6fb),
                                  Color(0xFF005bea),
                                ],
                                iconData: CustomIcons.linkedin,
                                onPressed: () {},
                              )
                            ],
                          ),
                          SizedBox(
                            height: ScreenUtil.getInstance().setHeight(30),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "New User? ",
                                style: TextStyle(fontFamily: "Poppins-Medium"),
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              SignupScreen()));
                                },
                                child: Text("SignUp",
                                    style: TextStyle(
                                        color: Color(0xFF5d74e3),
                                        fontFamily: "Poppins-Bold")),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            : Center(
                child: CircularProgressIndicator(
                  backgroundColor: Colors.red,
                ),
              ));
  }

  Future<void> googleLogin() async {
    String userId;
    final GoogleSignIn _googleSignIn = new GoogleSignIn();
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
        idToken: googleAuth.idToken, accessToken: googleAuth.accessToken);

    AuthResult user =
        await FirebaseAuth.instance.signInWithCredential(credential);

//    DocumentSnapshot ref = await Firestore.instance
//        .collection('users')
//        .document(user.user.uid)
//        .get();

//    print(ref.data['name']);
    await FirebaseAuth.instance.currentUser().then((user) {
      userId = user.uid;
    });
    await Firestore.instance.collection('users').document(userId).setData({
      'id': userId,
      'name': user.user.displayName,
      'pic': user.user.photoUrl
    });

    _saveDataLocal(user.user.uid, user.user.displayName, user.user.email,
        user.user.photoUrl, "googleLogin", 'deligate',"null");
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => MyHomePage()));
  }

  Future<void> login() async {
    final formState = _formkey.currentState;

    if (formState.validate()) {
      formState.save();
      setState(() {
        isLoading = true;
      });

      try {
        AuthResult user = await FirebaseAuth.instance
            .signInWithEmailAndPassword(email: _email, password: _password);
        DocumentSnapshot ref = await Firestore.instance
            .collection('users')
            .document(user.user.email)
            .get();

        print(ref.data['name']);

        _saveDataLocal(user.user.uid, ref.data['name'], user.user.email,
            ref.data['pic'], "emailLogin", ref.data['role'],ref.data['orderId']);

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MyHomePage()));

//      } catch (e) {
//        print(e.toString());
//      }

      } on AuthException catch (e) {
        _buildErrorDialog(context, e.message);
      } on Exception catch (e) {
        _buildErrorDialog(context, e.toString());
      }
    }
  }

  Future _buildErrorDialog(BuildContext context, _message) {
    return showDialog(
      builder: (context) {
        return AlertDialog(
          title: Text('Error Message'),
          content: Text(_message),
          actions: <Widget>[
            FlatButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {
                    isLoading = false;
                  });
                })
          ],
        );
      },
      context: context,
    );
  }

  _saveDataLocal(String userId, String userName, String email, String pic,
      String loginType, String role,String orderId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userId', userId);
    prefs.setString('userName', userName);
    prefs.setString('email', email);
    prefs.setString('pic', pic);
    prefs.setString('loginType', loginType);
    prefs.setString('role', role);
    prefs.setString('orderId', orderId);
  }
}

class CustomIcons {
  static const IconData twitter = IconData(0xe900, fontFamily: "CustomIcons");
  static const IconData facebook = IconData(0xe901, fontFamily: "CustomIcons");
  static const IconData googlePlus =
      IconData(0xe902, fontFamily: "CustomIcons");
  static const IconData linkedin = IconData(0xe903, fontFamily: "CustomIcons");
}

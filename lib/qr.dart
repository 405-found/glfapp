import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';

class Qr extends StatelessWidget {
  String qr;
  Qr({this.qr});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.transparent,
      appBar: AppBar(
        title: Text("Galle Literary Festival"),
        backgroundColor: Color(0xFF1b1e44),
      ),
      body: Center(
        child: Center(
          child: QrImage(
            data: qr,
            version: QrVersions.auto,
            size: 200.0,
          ),
        ),
      ),
    );
  }
}

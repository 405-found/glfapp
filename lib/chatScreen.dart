import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SingleChat extends StatefulWidget {
  String name;
  String peerid;
  String picture;
  String currentId;
  SingleChat({this.name, this.picture, this.peerid, this.currentId});

  @override
  _SingleChatState createState() => _SingleChatState();
}

class _SingleChatState extends State<SingleChat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.name),
          backgroundColor: Color(0xFF1b1e44),
        ),
        body: ChatScreen(
          name: widget.name,
          picture: widget.picture,
          peerid: widget.peerid,
          currentId: widget.currentId,
        ));
  }
}

class ChatScreen extends StatefulWidget {
  String name;
  String peerid;
  String picture;
  String currentId;
  ChatScreen({this.name, this.picture, this.peerid, this.currentId});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController _textController = new TextEditingController();

  String gropChatId;
  List<ChatMessage> _messages = [];

  String currentUserId;
  String currentUserName;
  String userPicture;

  void _handleSubmited(String text) {
    _addMessage(name: widget.name, text: text);

    _textController.clear();
  }

  void _getUserDetails() async {
    //get logged user details Ex name.id
    SharedPreferences prefs = await SharedPreferences.getInstance();
    currentUserName = prefs.getString('userName');
    userPicture=prefs.getString('pic');
  }

  void _generateChatId() {
    String peerId = widget.peerid;
    String currentUserId = widget.currentId;

    if (currentUserId.hashCode <= peerId.hashCode) {
      gropChatId = '$currentUserId-$peerId';
    } else {
      gropChatId = '$peerId-$currentUserId';
    }
    print(gropChatId);

    //g2eW64xl8rbzByRQotnTiz4STwE3-SPcwmhNmiGSFDqktSGPZKprmz0n2
    //

    //    final DocumentSnapshot result = await Firestore.instance
    //        .collection('messages')
    //        .document(gropChatId)
    //        .get();

    //    if (!result.exists) {
    //      Firestore.instance
    //          .collection('messages')
    //          .document(gropChatId)
    //          .collection(gropChatId)
    //          .add({});
//    }
  }

  void _addMessage({String name, String text}) {
    var sender = new ChatUser(name: name);
    var message = new ChatMessage(sender: sender, text: text);
    _sendMessage(text);
    print(message);
    setState(() {
      // print(message.text);
      _messages.insert(0, message);
    });
  }

  void _sendMessage(String text) async {
//    var currentUser = await FirebaseAuth.instance.currentUser();
//    var currentUserId = currentUser.uid;
//    var peerId = widget.peerid;

    var documentReference = await Firestore.instance
        .collection('messages')
        .document(gropChatId)
        .collection(gropChatId)
        .add({
      'content': text,
      'idFrom': widget.currentId,
      'idTo': widget.peerid,
      'timestamp': DateTime.now().millisecondsSinceEpoch.toString()
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _getUserDetails();
    _generateChatId();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8.0),
      child: StreamBuilder(
          stream: Firestore.instance
              .collection('messages')
              .document(gropChatId)
              .collection(gropChatId)
              .orderBy('timestamp', descending: true)
              .snapshots(),
          builder: (context, snapshot) {

            if (!snapshot.hasData) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(
                    backgroundColor: Colors.black87,
                  ),
                ),
              );
            }
            return Column(
              children: <Widget>[
                Flexible(
                  child: ListView.builder(
                    itemCount: snapshot.data.documents.length,
                    reverse: true,
                    padding: EdgeInsets.all(8.0),
                    itemBuilder: (context, i) {
                      if (snapshot.data.documents[i]['idFrom'] !=
                          widget.currentId)
                        return PeerMessage(
                          name: widget.name,
                          text: snapshot.data.documents[i]['content'],
                          picture: widget.picture,
                        );
                      else {
                        return UserMessage(
                          name: currentUserName,
                          text: snapshot.data.documents[i]['content'],
                          picture: userPicture,
                        );
                      }
                    },
                  ),
                ),
                Divider(
                  height: 1.0,
                ),
                Row(
                  children: <Widget>[
                    Flexible(
                      child: TextField(
                        controller: _textController,
                        onSubmitted: _handleSubmited,
                        decoration: InputDecoration(hintText: "Send a message"),
                      ),
                    ),
                    Container(
                      margin: new EdgeInsets.symmetric(horizontal: 4.0),
                      child: IconButton(
                        icon: Icon(Icons.send),
                        onPressed: () {
                          _handleSubmited(_textController.text);
                        },
                      ),
                    )
                  ],
                ),
              ],
            );
          }),
    );
  }
}

class PeerMessage extends StatelessWidget {
  String picture;
  String name;
  String text;
  PeerMessage({this.name, this.text, this.picture});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.end,
//        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(name, style: Theme.of(context).textTheme.subhead),
              new Container(
                margin: const EdgeInsets.only(top: 5.0),
//                                  child: _messages[i].msg != null
//                                      ? Text(_messages[i].msg)
//                                      : Text("Empty"),
                child: Text(text),
              ),
            ],
          ),
          new Container(
            margin: const EdgeInsets.only(right: 16.0),
            child: new CircleAvatar(
              backgroundImage: NetworkImage(picture),
            ),
          ),
        ],
      ),
    );
    ;
    ;
  }
}

class UserMessage extends StatelessWidget {
  String picture;
  String name;
  String text;
  UserMessage({this.name, this.text, this.picture});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10.0),
      child: new Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Container(
            margin: const EdgeInsets.only(right: 16.0),
            child: new CircleAvatar(
              backgroundImage: NetworkImage(picture),
            ),
          ),
          new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              new Text(name, style: Theme.of(context).textTheme.subhead),
              new Container(
                margin: const EdgeInsets.only(top: 5.0),
//                                  child: _messages[i].msg != null
//                                      ? Text(_messages[i].msg)
//                                      : Text("Empty"),
                child: Text(text),
              ),
            ],
          ),
        ],
      ),
    );
    ;
  }
}

class ChatMessage {
  ChatUser msgsender;
  String msg;

  ChatMessage({ChatUser sender, String text}) {
    msgsender = sender;
    msg = text;
  }
}

class ChatUser {
  String sendername;
  ChatUser({String name}) {
    sendername = name;
  }
}

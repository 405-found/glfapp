import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class TaskDetail extends StatefulWidget {
  final DocumentSnapshot post;
  TaskDetail({this.post});
  @override
  _TaskDetailState createState() => _TaskDetailState();
}

class _TaskDetailState extends State<TaskDetail> {


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        scrollDirection: Axis.vertical,
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 200.0,
            floating: true,
            pinned: false,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(
                widget.post.data['name'],
                style: TextStyle(color: Colors.black),
              ),
              background: Image.network(
                "http://galleliteraryfestival.com/wp-content/uploads/2018/12/fglf-2018-passes-and-tickets-guide-1180x440-180102-1-1-1.jpg",
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Container(
                child: Column(
                  children: <Widget>[
                    SafeArea(
                      child: SizedBox(
                        child: Container(
                          child: Card(
                            margin: EdgeInsets.all(20.0),
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  leading: Container(
                                    padding: EdgeInsets.only(right: 12.0),
                                    decoration: new BoxDecoration(
                                        border: new Border(
                                            right: new BorderSide(
                                                width: 1.0,
                                                color: Colors.white24))),
                                    child: Icon(
                                      Icons.person,
                                      color: Colors.blue,
                                      size: 50,
                                    ),
                                  ),
                                  title: Text(
                                    "mechanicName",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  // subtitle: Text(widget.price),
                                ),
                                Divider(
                                  color: Colors.black,
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  title: Text(
                                    "fsd",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                                ListTile(
                                  contentPadding: EdgeInsets.symmetric(
                                      horizontal: 20.0, vertical: 10.0),
                                  title: Text(
                                    "Job completed(7)",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      child: Container(
                        child: Card(
                          margin: EdgeInsets.all(20.0),
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                title: Text(
                                  "sdf",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                              ),
                              Divider(
                                color: Colors.black,
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.calendar_today,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text(
                                  "Date",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                // subtitle:
                              ),
                              ListTile(
                                contentPadding: EdgeInsets.symmetric(
                                    horizontal: 20.0, vertical: 10.0),
                                leading: Container(
                                  padding: EdgeInsets.only(right: 12.0),
                                  decoration: new BoxDecoration(
                                      border: new Border(
                                          right: new BorderSide(
                                              width: 1.0,
                                              color: Colors.white24))),
                                  child: Icon(
                                    Icons.timelapse,
                                    color: Colors.blue,
                                    size: 30,
                                  ),
                                ),
                                title: Text(
                                  "Time",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                // subtitle: Text(time),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   child: Container(
                    //     child: Card(
                    //       margin: EdgeInsets.all(20.0),
                    //       child: Column(
                    //         children: <Widget>[
                    //           TextField(
                    //             onChanged: (value) {
                    //               // userNote = value;
                    //             },
                    //             decoration: InputDecoration(
                    //                 border: InputBorder.none,
                    //                 hintText: 'Add a note to the mechanic'),
                    //           ),
                    //         ],
                    //       ),
                    //     ),
                    //   ),
                    // ),
                    Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          padding: const EdgeInsets.only(
                            left: 70.0,
                            right: 70.0,
                          ),
                          elevation: 10.0,
                          onPressed: () {
                            setState(() {
                              // toggle = false;
                            });
                            // http.post(url, body: {
                            //   "token": widget.deviceToken,
                            //   // "pass": password,
                            //   "key": "request",
                            // });
                            // _makeBooking();
                            // _neverSatisfied();
                          },
                          color: Colors.green,
                          // color: toggle ? Colors.green : Colors.grey,
                          child: Text(
                            "Done",
                            style: TextStyle(color: Colors.white),
                          ),
                        )),
                  ],
                ),
              );
            },
            childCount: 1,
          ))
        ],
      ),
    );
  }
}

import 'dart:io';

import 'package:GLF/taskDetails.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class TaskMenu extends StatefulWidget {
  @override
  _TaskMenuState createState() => _TaskMenuState();
}

Future getTask() async {
  var firestore = Firestore.instance;
  QuerySnapshot qn = await firestore.collection('events').getDocuments();
  return qn.documents;
}

class _TaskMenuState extends State<TaskMenu> {
  final Firestore _db = Firestore.instance;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  _saveDeviceToken() async {
    // Get the current user
    String email = '';
    await FirebaseAuth.instance.currentUser().then((user) {
      email = user.email;
    });

    // await FirebaseUser user = await _auth.currentUser();

    // Get the token for this device
    String fcmToken = await _fcm.getToken();

    // Save it to Firestore
    if (fcmToken != null) {
      var tokens = _db
          .collection('users')
          .document(email)
          .collection('tokens')
          .document(fcmToken);

      await tokens.setData({
        'token': fcmToken,
        'createdAt': FieldValue.serverTimestamp(), // optional
        'platform': Platform.operatingSystem // optional
      });
    }
  }

  @override
  void initState() {
    _saveDeviceToken();

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF1b1e44),
        title: Text("Tasks"),
      ),
      body: Container(
        child: FutureBuilder(
            future: getTask(),
            builder: (_, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: Text("Loading"),
                );
              } else {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (_, index) {
                      return Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                          // child: FittedBox(
                          child: Material(
                            color: Colors.white,
                            elevation: 14.0,
                            borderRadius: BorderRadius.circular(5.0),
                            shadowColor: Colors.grey,
                            child: ListTile(
                              trailing: RaisedButton(
                                onPressed: () {},
                                textColor: Colors.white,
                                padding: const EdgeInsets.all(0.0),
                                child: Container(
                                  decoration: const BoxDecoration(
                                    gradient: LinearGradient(
                                      colors: <Color>[
                                        Color(0xFF0D47A1),
                                        Color(0xFF1976D2),
                                        Color(0xFF42A5F5),
                                      ],
                                    ),
                                  ),
                                  padding: const EdgeInsets.all(10.0),
                                  child: const Text('Accept',
                                      style: TextStyle(fontSize: 20)),
                                ),
                              ),
                              title: Text(
                                snapshot.data[index].data['name']
                                    .toString()
                                    .toUpperCase(),
                                style: TextStyle(
                                    fontFamily: 'MonteSerrat',
                                    fontWeight: FontWeight.bold),
                              ),
                              subtitle: Text(snapshot.data[index].data['name']),
                              onTap: () {
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => TaskDetail(
                                              post: snapshot.data[index],
                                            )));
                              },
                            ),
                          ),
                          // ),
                        ),
                      );
                    });
              }
            }),
      ),
    );
  }
}

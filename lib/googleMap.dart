import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';



class GoogleMapPage extends StatefulWidget {
  @override
  _GoogleMapPageState createState() => _GoogleMapPageState();
}

class _GoogleMapPageState extends State<GoogleMapPage> {
  List<Marker> allMarkers = [];

  

  @override
  void initState() {
    allMarkers.add(Marker(
        markerId: MarkerId('myMarker'),
        onTap: () {
          print('Marker Tap');
        },
        position: LatLng(6.029270, 80.215124),
        draggable: false));
    super.initState();
  }

  Completer<GoogleMapController> _controller = Completer();

    Location location = new Location();


  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(6.029270, 80.215124),
    
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(37.43296265331129, -122.08832357078792),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  Widget button(Function function, IconData icon, Color color) {
    return FloatingActionButton(
      heroTag: color.toString(),
      onPressed: function,
      materialTapTargetSize: MaterialTapTargetSize.padded,
      backgroundColor: color,
      child: Icon(
        icon,
        size: 36.0,
      ),
    );
  }

  Future _allEvents() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection('events').getDocuments();
    // print(qn.documents[0]['name']);
    for (var item in qn.documents) {
      print(item['location'].latitude);
      setState(() {
        allMarkers = [];
        allMarkers.add(Marker(
            markerId: MarkerId(item['name']),
            position:
                LatLng(item['location'].latitude, item['location'].longitude)));
      });
    }
  }

  Future _myEvents() async {
    var firestore = Firestore.instance;
    QuerySnapshot qn = await firestore.collection('events').getDocuments();
    // print(qn.documents[0]['name']);
    for (var item in qn.documents) {
      print(item['location'].latitude);
      setState(() {
        allMarkers = [];
        allMarkers.add(Marker(
            markerId: MarkerId("TEST"), position: LatLng(6.129270, 80.215124)));
      });
    }
  }

  _freeEvents() {}

  @override

  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Nearby Events"),
        backgroundColor: Color(0xFF1b1e44),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            mapType: MapType.normal,
            initialCameraPosition:
                CameraPosition(target: LatLng(6.029270, 80.215124), zoom: 16),
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
            markers: Set.from(allMarkers),
            myLocationEnabled: true,
          ),

          Padding(
            padding: EdgeInsets.all(16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: Column(
                children: <Widget>[
                  button(_allEvents, Icons.all_inclusive, Colors.blue),
                  SizedBox(
                    height: 16.0,
                  ),
                  button(_myEvents, Icons.account_balance, Colors.green),
                  SizedBox(
                    height: 16.0,
                  ),
                  button(_freeEvents, Icons.free_breakfast, Colors.red),
                ],
              ),
            ),
          ),
//          Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: Align(
//                alignment: Alignment.bottomRight,
//                child: Column(
//                  children: <Widget>[
//                    FloatingActionButton.extended(
//                      label: Text('All Events',style: TextStyle(fontSize: 12),),
//                      icon: Icon(Icons.all_inclusive),
//                    ),
//                    SizedBox(height: 16.0),
//                    FloatingActionButton.extended(
//                      label: Text('My Events',style: TextStyle(fontSize: 12),),
//                      icon: Icon(Icons.event_note),
//                      backgroundColor: Colors.green,
//                    ),
//                    SizedBox(
//                      height: 16.0,
//                    ),
//                    FloatingActionButton.extended(
//                      tooltip: "Free",
//                      label: Text('Free Events',style: TextStyle(fontSize: 12),),
//                      icon: Icon(Icons.open_in_browser),
//                      backgroundColor: Colors.red,
//                    ),
//                  ],
//                )),
//          ),
        ],
      ),

      // floatingActionButton: FloatingActionButton.extended(
      //   onPressed: () {},
      //   label: Text('Free Events'),
      //   icon: Icon(Icons.event_available),
      // ),
    );
  }
}

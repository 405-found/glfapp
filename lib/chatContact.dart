import 'package:GLF/chatScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Message extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MessageState();
  }
}

class _MessageState extends State<Message> {

  String userId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserId();
  }

  _getUserId() async{
    SharedPreferences prefs= await SharedPreferences.getInstance();
    userId=prefs.getString('userId');
  }

//  void _generateChatId() async {
//    SharedPreferences prefs= await SharedPreferences.getInstance();
//    String currentUserId=prefs.getString('userId');
//    var peerId = widget.peerid;
//
//    var gropChatId;
//
//    if (currentUserId.hashCode <= peerId.hashCode) {
//      gropChatId = '$currentUserId-$peerId';
//    } else {
//      gropChatId = '$currentUserId-$peerId';
//    }
//    print(gropChatId);
//
//    final DocumentSnapshot result = await Firestore.instance
//        .collection('messages')
//        .document(gropChatId)
//        .get();
//    if (!result.exists) {
//      Firestore.instance.collection('messages').document(gropChatId).collection(gropChatId).add({'text':'sex'});
//    }
//  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    print(Firestore.instance.collection('users').snapshots());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF1b1e44),
        title: Text("Chats"),
      ),
      body: StreamBuilder(
        stream: Firestore.instance.collection('users').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container(
              child: Center(
                child: Text("Empty"),
              ),
            );
          }
          return ListView.builder(
              itemCount: snapshot.data.documents.length,
              itemBuilder: (context, index) {
                return snapshot.data.documents[index]['id'] !=
                    userId
                    ?  Column(
                        children: <Widget>[
                          Divider(
                            height: 12.0,
                          ),
                          ListTile(
                            leading: CircleAvatar(
                              radius: 24.0,
                              backgroundImage:
                                  NetworkImage(snapshot.data.documents[index]['pic']),
                            ),
                            title: Row(
                              children: <Widget>[
                                Text(snapshot.data.documents[index]['name']),
                                SizedBox(
                                  width: 16.0,
                                ),
                                // Text(
                                //   dummyData[index].datetime,
                                //   style: TextStyle(fontSize: 12.0),
                                // ),
                              ],
                            ),
                            // subtitle: Text(dummyData[index].message),
                            trailing: Icon(
                              Icons.arrow_forward_ios,
                              size: 14.0,
                            ),
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SingleChat(
                                          name: snapshot.data.documents[index]
                                              ['name'],
                                          peerid: snapshot.data.documents[index]
                                              ['id'],
                                          picture: snapshot.data.documents[index]
                                          ['pic'],
                                          currentId:userId,
                                        ))),
                          ),
                        ],
                      )
                    : Container();
              });
        },
      ),
      floatingActionButton: FloatingActionButton(
          elevation: 0.0,
          child: new Icon(Icons.add),
          backgroundColor: new Color(0xFF2d3447),
          onPressed: () {}),
    );
  }
}

class ChatModel {
  final String avatarUrl;
  final String name;
  final String datetime;
  final String message;

  ChatModel({this.avatarUrl, this.name, this.datetime, this.message});
}

final List<ChatModel> dummyData = [
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/women/34.jpg",
    name: "Laurent",
    datetime: "20:18",
    message: "How about meeting tomorrow?",
  ),
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/women/49.jpg",
    name: "Tracy",
    datetime: "19:22",
    message: "I love that idea, it's great!",
  ),
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/women/77.jpg",
    name: "Claire",
    datetime: "14:34",
    message: "I wasn't aware of that. Let me check",
  ),
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/81.jpg",
    name: "Joe",
    datetime: "11:05",
    message: "Flutter just release 1.0 officially. Should I go for it?",
  ),
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/83.jpg",
    name: "Mark",
    datetime: "09:46",
    message: "It totally makes sense to get some extra day-off.",
  ),
  ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/85.jpg",
    name: "Williams",
    datetime: "08:15",
    message: "It has been re-scheduled to next Saturday 7.30pm",
  ), ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/85.jpg",
    name: "Williams",
    datetime: "08:15",
    message: "It has been re-scheduled to next Saturday 7.30pm",
  ), ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/85.jpg",
    name: "Williams",
    datetime: "08:15",
    message: "It has been re-scheduled to next Saturday 7.30pm",
  ), ChatModel(
    avatarUrl: "https://randomuser.me/api/portraits/men/85.jpg",
    name: "Williams",
    datetime: "08:15",
    message: "It has been re-scheduled to next Saturday 7.30pm",
  ),
];

import 'dart:async';

import 'package:GLF/Entry/loginScreen.dart';
import 'package:GLF/chatContact.dart';
import 'package:GLF/qr.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geoflutterfire/geoflutterfire.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'dart:math';
import 'taskMenu.dart';
import 'package:location/location.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'googleMap.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key) {}

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

var cardAspectRatio = 12.0 / 16.0;
var widgetAspectRatio = cardAspectRatio * 1.2;

class _MyHomePageState extends State<MyHomePage> {
  var currentPage = images.length - 1.0;
  String currentUserName = "";
  String email = "";
  String pic =
      "https://archive.org/download/user-image-with-black-background_318-34564/user-image-with-black-background_318-34564.jpg";
  String loginType;
  String role = "";
  String orderId = "";

  Location location = new Location();

  Firestore firestore = Firestore.instance;
  Geoflutterfire geo = Geoflutterfire();

  // Future _addGeoPoint() async {
  Future<DocumentReference> _addGeoPoint() async {
    var pos = await location.getLocation();
    GeoFirePoint point =
        geo.point(latitude: pos.latitude, longitude: pos.longitude);
    // return point;

    if (role == 'author') {
      firestore
          .collection('authors')
          .document(email)
          .updateData({'latitude': pos.latitude, 'longitude': pos.longitude});
    } else {
      firestore
          .collection('users')
          .document(email)
          .updateData({'location': point.data});
    }

    // .add({'position': point.data, 'name': 'Yay I can be queried!'});
  }

  Timer timer;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUserDetails();
    timer = Timer.periodic(Duration(seconds: 10), (Timer t) => _addGeoPoint());
  }

  void _getUserDetails() async {
    //get logged user details Ex name.id
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      currentUserName = prefs.getString('userName');
      email = prefs.getString('email');
      pic = prefs.getString('pic');
      role = prefs.getString('role');
      orderId = prefs.getString('orderId');
    });
    loginType = prefs.get('loginType');
  }

  @override
  Widget build(BuildContext context) {
    PageController controller = PageController(initialPage: images.length - 1);
    controller.addListener(() {
      setState(() {
        currentPage = controller.page;
      });
    });
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
            Color(0xFF1b1e44),
            Color(0xFF2d3447),
          ],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
              tileMode: TileMode.clamp)),
      child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            title: Text("Galle Literary Festival"),
            backgroundColor: Colors.transparent,
          ),
          drawer: Drawer(
            child: ListView(
              children: <Widget>[
                UserAccountsDrawerHeader(
                  accountName: Text(currentUserName),
                  accountEmail: Text(email),
                  currentAccountPicture: Image.network(pic),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [
                        Color(0xFF1b1e44),
                        Color(0xFF2d3447),
                      ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                          tileMode: TileMode.clamp)),
                ),
                ListTile(
                    title: Text("Chat"),
                    trailing: Icon(Icons.message),
                    onTap: () {
                      Navigator.pop(context);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Message()));
                    }),
                ListTile(
                  title: Text("Map"),
                  trailing: Icon(Icons.map),
                  onTap: () {
                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GoogleMapPage()));
                  },
                ),
                role == "volunteer"
                    ? ListTile(
                        title: Text("Task"),
                        trailing: Icon(Icons.add_box),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TaskMenu()));
                        },
                      )
                    : Container(),
                role == "deligate"
                    ? ListTile(
                        title: Text("QR ID"),
                        trailing: Icon(Icons.blur_on),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Qr(
                                        qr: orderId,
                                      )));
                        },
                      )
                    : Container(),
                ListTile(
                  title: Text("Log out"),
                  onTap: logout,
                )
              ],
            ),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 12.0, right: 12.0, top: 30.0, bottom: 8.0),
                  // child: Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: <Widget>[
                  //     IconButton(
                  //       icon: Icon(
                  //         CustomIcons.menu,
                  //         color: Colors.white,
                  //         size: 30.0,
                  //       ),
                  //       onPressed: () {},
                  //     ),

                  //     IconButton(
                  //       icon: Icon(
                  //         Icons.search,
                  //         color: Colors.white,
                  //         size: 30.0,
                  //       ),
                  //       onPressed: () {},
                  //     )
                  //   ],
                  // ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Upcoming",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 46.0,
                            // fontFamily: "Calibre-Semibold",
                            letterSpacing: 1.0,
                          )),
                      IconButton(
                        icon: Icon(
                          CustomIcons.option,
                          size: 12.0,
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Color(0xFFff6e6e),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22.0, vertical: 6.0),
                            child: Text("Trending",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text("25+ Events",
                          style: TextStyle(color: Colors.blueAccent))
                    ],
                  ),
                ),
                Stack(
                  children: <Widget>[
                    CardScrollWidget(currentPage),
                    Positioned.fill(
                      child: PageView.builder(
                        itemCount: images.length,
                        controller: controller,
                        reverse: true,
                        itemBuilder: (context, index) {
                          return Container();
                        },
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("Favourite",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 46.0,
                            // fontFamily: "Calibre-Semibold",
                            letterSpacing: 1.0,
                          )),
                      IconButton(
                        icon: Icon(
                          CustomIcons.option,
                          size: 12.0,
                          color: Colors.white,
                        ),
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.blueAccent,
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: Center(
                          child: Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 22.0, vertical: 6.0),
                            child: Text("Latest",
                                style: TextStyle(color: Colors.white)),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      Text("9+ Events",
                          style: TextStyle(color: Colors.blueAccent))
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(left: 18.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20.0),
                        child: Image.asset("assets/image_02.jpg",
                            width: 296.0, height: 222.0),
                      ),
                    ),
                  ],
                )
              ],
            ),
          )),
    );
  }

  Future<void> logout() async {
    if (loginType == "emailLogin") {
      FirebaseAuth.instance.signOut();
    } else if (loginType == "googleLogin") {
      final GoogleSignIn _googleSignIn = GoogleSignIn();
      _googleSignIn.signOut();
    }

    final pref = await SharedPreferences.getInstance();
    await pref.clear();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LoginScreen()));
  }
}

class CustomIcons {
  static const IconData menu = IconData(0xe900, fontFamily: "Custom-Icons");
  static const IconData option = IconData(0xe902, fontFamily: "Custom-Icons");
}

List<String> images = [
  "assets/image_04.jpg",
  "assets/image_03.jpg",
  "assets/image_02.jpg",
  "assets/image_01.jpg",
];

List<String> title = [
  "Spa Ceylon",
  "Lucy Fleming & Anthony ",
  "Literary Dinner",
  "Red Birds",
];

class CardScrollWidget extends StatelessWidget {
  var currentPage;
  var padding = 20.0;
  var verticalInset = 20.0;

  CardScrollWidget(this.currentPage);

  @override
  Widget build(BuildContext context) {
    return new AspectRatio(
      aspectRatio: widgetAspectRatio,
      child: LayoutBuilder(builder: (context, contraints) {
        var width = contraints.maxWidth;
        var height = contraints.maxHeight;

        var safeWidth = width - 2 * padding;
        var safeHeight = height - 2 * padding;

        var heightOfPrimaryCard = safeHeight;
        var widthOfPrimaryCard = heightOfPrimaryCard * cardAspectRatio;

        var primaryCardLeft = safeWidth - widthOfPrimaryCard;
        var horizontalInset = primaryCardLeft / 2;

        List<Widget> cardList = new List();

        for (var i = 0; i < images.length; i++) {
          var delta = i - currentPage;
          bool isOnRight = delta > 0;

          var start = padding +
              max(
                  primaryCardLeft -
                      horizontalInset * -delta * (isOnRight ? 15 : 1),
                  0.0);

          var cardItem = Positioned.directional(
            top: padding + verticalInset * max(-delta, 0.0),
            bottom: padding + verticalInset * max(-delta, 0.0),
            start: start,
            textDirection: TextDirection.rtl,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16.0),
              child: Container(
                decoration: BoxDecoration(color: Colors.white, boxShadow: [
                  BoxShadow(
                      color: Colors.black12,
                      offset: Offset(3.0, 6.0),
                      blurRadius: 10.0)
                ]),
                child: AspectRatio(
                  aspectRatio: cardAspectRatio,
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Image.asset(images[i], fit: BoxFit.cover),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 8.0),
                              child: Text(title[i],
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 25.0,
                                    // fontFamily: "SF-Pro-Text-Regular"
                                  )),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 12.0, bottom: 12.0),
                              child: Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 22.0, vertical: 6.0),
                                decoration: BoxDecoration(
                                    color: Colors.blueAccent,
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: Text("Read Later",
                                    style: TextStyle(color: Colors.white)),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
          cardList.add(cardItem);
        }
        return Stack(
          children: cardList,
        );
      }),
    );
  }
}

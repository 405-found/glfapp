import 'package:GLF/Entry/loginScreen.dart';
import 'package:GLF/HomePage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';


class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    navigateScreen(context);
  }

  navigateScreen(BuildContext context) async{
    String loginStatus;
    await SharedPreferences.getInstance().then((prefs){
      loginStatus=prefs.getString('loginType');
    });

    if (loginStatus=="googleLogin" || loginStatus=="emailLogin") {
      // wrong call in wrong place!
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => MyHomePage(),
      ));
    } else {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.red,
        ),
      ),
    );
  }




}
